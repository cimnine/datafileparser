﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Collections.Generic;
using System.Text;
using ch.f00.DataFileParser.Exceptions;

namespace ch.f00.DataFileParser.CSV
{
    [TestClass]
    public class CSVDataFileParserTest
    {
        CSVDataFileParser<TestPerson> sut;
        TextReader reader;

        [TestInitialize]
        public void SetUp()
        {
            sut = new CSVDataFileParser<TestPerson>(';');
        }

        [TestMethod]
        public void TestSimpleParseWithIrregularEnding()
        {
            reader = new StreamReader(@"CSV\DummyDataA.csv", Encoding.Default);
            IEnumerable<TestPerson> result = sut.ParseFile(reader);
            Assert.IsNotNull(result);

            int i = 0;
            foreach (TestPerson p in result)
            {
                Assert.IsNotNull(p);
                i++;
            }
            Assert.IsTrue(i == 2);
        }

        [TestMethod]
        public void TestSimpleParse()
        {
            reader = new StreamReader(@"CSV\DummyDataC.csv", Encoding.Default);
            IEnumerable<TestPerson> result = sut.ParseFile(reader);
            Assert.IsNotNull(result);

            int i = 0;
            foreach (TestPerson p in result)
            {
                Assert.IsNotNull(p);
                i++;
            }
            Assert.IsTrue(i == 2);
        }

        [TestMethod]
        public void TestSimpleParseWithCommentedLines()
        {
            reader = new StreamReader(@"CSV\DummyDataD.csv", Encoding.Default);
            IEnumerable<TestPerson> result = sut.ParseFile(reader);
            Assert.IsNotNull(result);

            int i = 0;
            foreach (TestPerson p in result)
            {
                Assert.IsNotNull(p);
                i++;
            }
            Assert.IsTrue(i == 2);
        }

        [TestMethod]
        public void TestSimpleParseWithDateLines()
        {
            reader = new StreamReader(@"CSV\DummyDataB.csv", Encoding.Default);
            IEnumerable<TestPerson> result = sut.ParseFile(reader);
            Assert.IsNotNull(result);

            int i = 0;
            foreach (TestPerson p in result)
            {
                Assert.IsNotNull(p);
                i++;
            }
            Assert.IsTrue(i == 2);
        }

        [TestMethod]
        public void TestSimpleParseWithQuotedLines()
        {
            reader = new StreamReader(@"CSV\DummyDataE.csv", Encoding.Default);
            IEnumerable<TestPerson> result = sut.ParseFile(reader);
            Assert.IsNotNull(result);

            int i = 0;
            foreach (TestPerson p in result)
            {
                Assert.IsNotNull(p);
                if (i == 0)
                {
                    Assert.IsTrue(p.Vorname.IndexOf(';') > 0);
                }
                else if (i == 1)
                {
                    Assert.IsTrue(p.Vorname.IndexOf('"') > 0);
                }
                i++;
            }
            Assert.IsTrue(i == 3);
        }

        [TestMethod]
        public void TestSimpleParseWithoutSkipComments()
        {
            sut.SkipComments = false;
            reader = new StreamReader(@"CSV\DummyDataD.csv", Encoding.Default);
            IEnumerable<TestPerson> result = sut.ParseFile(reader);
            Assert.IsNotNull(result);

            int i = 0;
            foreach (TestPerson p in result)
            {
                Assert.IsNotNull(p);
                if (i == 0)
                {
                    Assert.IsTrue(p.Name.StartsWith("#"));
                }
                else if (i == 2)
                {
                    Assert.IsTrue(p.Name.StartsWith("//"));
                }
                else if (i == 4)
                {
                    Assert.IsTrue(p.Name.StartsWith("'"));
                }
                i++;
            }
            Assert.IsTrue(i == 5);
        }

        [TestMethod]
        public void TestLineParseErrorReporting()
        {
            reader = new StreamReader(@"CSV\DummyDataG.csv", Encoding.Default);
            sut.ParseFile(reader);

            int i = 0;
            foreach (var err in sut.ParsingErrors)
            {
                Assert.IsNotNull(err);
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(err.LineNumber == 1);
                        break;
                    case 1:
                        Assert.IsTrue(err.LineNumber == 3);
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 2);

            TextReader reader2 = new StreamReader(@"CSV\DummyDataA.csv", Encoding.Default);
            sut.ParseFile(reader2);
            Assert.IsTrue(sut.ParsingErrors.Count == 0);
        }

        [TestMethod, ExpectedException(typeof(FileHeaderNotInMemberException))]
        public void TestFileHeaderMissmatch()
        {
            sut.ThrowOnFileHeadersMissmatch = true;
            reader = new StreamReader(@"CSV\DummyDataF.csv", Encoding.Default);
            sut.ParseFile(reader);
        }

        [TestMethod, ExpectedException(typeof(MemberNotInFileHeaderException))]
        public void TestMemberMissmatch()
        {
            sut.ThrowOnTypeMembersMissmatch = true;
            reader = new StreamReader(@"CSV\DummyDataC.csv", Encoding.Default);
            sut.ParseFile(reader);
        }
    }
}
