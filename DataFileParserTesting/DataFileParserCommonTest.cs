﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using ch.f00.DataFileParser.Attributes;

namespace ch.f00.DataFileParser
{
    [TestClass]
    public class DataFileParserCommonTest
    {
        DataFileParserCommonStub<object> sut;
        String tmpFileName;

        [TestInitialize]
        public void SetUp()
        {
            sut = new DataFileParserCommonStub<object>();
            tmpFileName = Path.GetTempFileName();
        }

        [TestMethod]
        public void TestParseFileWithValidFile()
        {
            sut.ParseFile(tmpFileName);
            Assert.IsTrue(sut.ParseFileHitCount == 1);
        }

        [TestMethod, ExpectedException(typeof(FileNotFoundException))]
        public void TestParseFileWithInvalidFile()
        {
            sut.ParseFile(@"C:\ThatDoesNotExist.txt");
        }

        [TestMethod]
        public void TestParseFileWithValidFileAndNullProccessor()
        {
            sut.ParseFile(tmpFileName, (Func<object, object>)null);
            Assert.IsTrue(sut.ParseFileHitCount == 1);
        }

        [TestMethod]
        public void TestParseFileWithValidFileAndNotNullProccessor()
        {
            sut.ParseFile(tmpFileName, (Func<object, object>)null);
            Assert.IsTrue(sut.ParseFileHitCount == 1);
        }

        [TestMethod, ExpectedException(typeof(FileNotFoundException))]
        public void TestParseFileWithInvalidFileAndNullProcessor()
        {
            sut.ParseFile(@"C:\ThatDoesNotExist.txt", (Func<object, object>)null);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public void TestParseFileWithValidFileAndNullConvertor()
        {
            sut.ParseFile(tmpFileName, (Func<Dictionary<String, String>, object>)null);
            Assert.IsTrue(sut.ParseFileHitCount == 1);
        }

        [TestMethod]
        public void TestParseFileWithValidFileAndNotNullConvertor()
        {
            sut.ParseFile(tmpFileName, (Func<Dictionary<String, String>, object>)(x => x));
            Assert.IsTrue(sut.ParseFileHitCount == 1);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public void TestParseFileWithInvalidFileAndNullConvertor()
        {
            sut.ParseFile(@"C:\ThatDoesNotExist.txt", (Func<Dictionary<String, String>, object>)null);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public void TestParseFileWithNullFileAndNullConvertor()
        {
            sut.ParseFile((String)null, (Func<Dictionary<String, String>, object>)((x) => default(object)));
        }

        [TestMethod, ExpectedException(typeof(FileNotFoundException))]
        public void TestParseFileWithInvalidFileAndConvertor()
        {
            sut.ParseFile(@"C:\ThatDoesNotExist.txt", (Func<Dictionary<String, String>, object>)((x) => default(object)));
        }

        [TestMethod]
        public void TestReadUntilData()
        {
            String testData = "One\nTwo\nThree\nFour\nFive";
            int linecount = 0;
            TextReader reader = new StringReader(testData);

            String result = sut.ReadUntilDataProxy(ref linecount, reader);

            Console.WriteLine(linecount);
            Assert.AreEqual("One", result);
            Assert.AreEqual(0, linecount);

            result = sut.ReadUntilDataProxy(ref linecount, reader);

            Assert.AreEqual("Two", result);
            Assert.AreEqual(1, linecount + 1);
        }

        [TestMethod]
        public void TestReadUntilDataWithSkips()
        {
            String testData = "One\n#Two\n//Three\n'Four\nFive";
            int linecount = 0;
            TextReader reader = new StringReader(testData);

            String result = sut.ReadUntilDataProxy(ref linecount, reader);

            Console.WriteLine(linecount);
            Assert.AreEqual("One", result);
            Assert.AreEqual(0, linecount);

            result = sut.ReadUntilDataProxy(ref linecount, reader);

            Assert.AreEqual("Five", result);
            Assert.AreEqual(4, linecount + 1);
        }

        [TestMethod]
        public void TestGetFields()
        {
            var expectation = new Dictionary<String, FieldInfo>
            {
                { "Name", typeof(TestPerson).GetField("Name") },
                { "Vorname", typeof(TestPerson).GetField("Vorname") },
                { "Gehalt" ,typeof(TestPerson).GetField("Gehalt") }
            };
            var result = new DataFileParserCommonStub<TestPerson>().GetPropertiesProxy();
            Assert.IsTrue(result.ContainsKey("Name"));
            Assert.IsTrue(result.ContainsKey("Vorname"));
            Assert.IsTrue(result.ContainsKey("Gehalt"));
            Assert.IsTrue(result.ContainsKey("Geburtstag"));
            Assert.IsTrue(result.ContainsValue(typeof(TestPerson).GetProperty("Name")));
            Assert.IsTrue(result.ContainsValue(typeof(TestPerson).GetProperty("Vorname")));
            Assert.IsTrue(result.ContainsValue(typeof(TestPerson).GetProperty("Gehalt")));
            Assert.IsTrue(result.ContainsValue(typeof(TestPerson).GetProperty("Geburtsdatum")));
        }
    }

    public class DataFileParserCommonStub<T> : DataFileParser<T> where T : class, new()
    {
        public DataFileParserCommonStub()
            : base()
        {
        }

        public int ParseFileHitCount { get; private set; }

        public string ReadUntilDataProxy(ref int lineCounter, TextReader input)
        {
            return ReadUntilData(ref lineCounter, input);
        }

        public Dictionary<String, PropertyInfo> GetPropertiesProxy()
        {
            return GetProperties();
        }

        public override IEnumerable<T> ParseFile(TextReader input, Func<T, T> proccessor = null)
        {
            ParseFileHitCount++;
            return default(IEnumerable<T>);
        }

        public override IEnumerable<T> ParseFile(TextReader input, Func<Dictionary<string, string>, T> creator)
        {
            ParseFileHitCount++;
            return default(IEnumerable<T>);
        }
    }

    class TestPerson
    {
        public String Name { get; set; }
        public String Vorname { get; set; }

        [ColumnName("Geburtstag"), Timestamp]
        public DateTime Geburtsdatum { get; set; }
        public float Gehalt { get; set; }
    }
}
