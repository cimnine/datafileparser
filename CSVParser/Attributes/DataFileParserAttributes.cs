﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ch.f00.DataFileParser.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ColumnName : System.Attribute
    {
        public ColumnName(String alternativeColumnName)
        {
            AlternativeColumnName = alternativeColumnName;
        }

        public String AlternativeColumnName { get; private set; }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class DateTimeFormat : System.Attribute
    {
        public DateTimeFormat(IFormatProvider formatProvider)
        {
            FormatProvider = formatProvider;
        }

        public IFormatProvider FormatProvider { get; private set; }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class Timestamp : System.Attribute
    {
    }
}
