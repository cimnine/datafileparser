﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ch.f00.DataFileParser.Exceptions
{
    [Serializable]
    public class UnknownFieldException : Exception
    {
        public UnknownFieldException() { }
        public UnknownFieldException(string message) : base(message) { }
        public UnknownFieldException(string message, Exception inner) : base(message, inner) { }
        protected UnknownFieldException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
