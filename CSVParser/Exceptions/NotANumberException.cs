﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ch.f00.DataFileParser.Exceptions
{
    [Serializable]
    public class NotANumberException : Exception
    {
        public NotANumberException() { }
        public NotANumberException(string message) : base(message) { }
        public NotANumberException(string message, Exception inner) : base(message, inner) { }
        protected NotANumberException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
