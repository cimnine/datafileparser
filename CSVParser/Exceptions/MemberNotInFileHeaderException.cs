﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ch.f00.DataFileParser.Exceptions
{
    [Serializable]
    public class MemberNotInFileHeaderException : Exception
    {
        public MemberNotInFileHeaderException(string member) : base(member) { }
        public MemberNotInFileHeaderException(string member, string message) : base(member + " " + message) { }
        public MemberNotInFileHeaderException(string member, string message, Exception inner) : base(member + " " + message, inner) { }
        protected MemberNotInFileHeaderException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
