﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ch.f00.DataFileParser.Exceptions
{
    [Serializable]
    public class UnknownFiletypeException : Exception
    {
        public UnknownFiletypeException() { }
        public UnknownFiletypeException(string message) : base(message) { }
        public UnknownFiletypeException(string message, Exception inner) : base(message, inner) { }
        protected UnknownFiletypeException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
