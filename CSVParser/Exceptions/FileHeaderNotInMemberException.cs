﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ch.f00.DataFileParser.Exceptions
{
    [Serializable]
    public class FileHeaderNotInMemberException : Exception
    {
        public FileHeaderNotInMemberException(string header) : base(header) { }
        public FileHeaderNotInMemberException(string header, string message) : base(header + " " + message) { }
        public FileHeaderNotInMemberException(string header, string message, Exception inner) : base(header + " " + message, inner) { }
        protected FileHeaderNotInMemberException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
