﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ch.f00.DataFileParser.Errors
{
    public class LineError
    {
        public int LineNumber { get; set; }
        public String AffectedLine { get; set; }
        public Exception RelatedException { get; set; }
    }
}
