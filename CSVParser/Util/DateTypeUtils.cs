﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ch.f00.DataFileParser.Util
{
    public static class DateTypeUtils
    {
        public static bool IsDate(this Type type)
        {
            return type.Equals(typeof(DateTime));
        }

        public static DateTime ParseDate(this String value)
        {
            return DateTime.Parse(value);
        }

        public static DateTime ParseTimestamp(this String value)
        {
            return new System.DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(Convert.ToInt32(value));
        }

        public static DateTime ParseDate(this String value, IFormatProvider provider)
        {
            return DateTime.Parse(value, provider);
        }
    }
}
