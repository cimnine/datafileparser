﻿using ch.f00.DataFileParser.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ch.f00.DataFileParser.Util
{
    public static class IsNumericType
    {
        public static bool IsNumeric(this Type type)
        {
            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Single:
                    return true;
                case TypeCode.Object:
                    if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        return Nullable.GetUnderlyingType(type).IsNumeric();
                        //return IsNumeric(Nullable.GetUnderlyingType(type));
                    }
                    return false;
                default:
                    return false;
            }
        }
    }

    public static class NumericConverter
    {
        public static object ParseNumber<T>(this String s) where T : Type, new()
        {
            if (!typeof(T).IsNumeric())
            {
                throw new ArgumentException("T must be a numeric type!");
            }
            return ParseNumber(s, typeof(T));
        }

        public static object ParseNumber(this String s, Type type)
        {
            if (!type.IsNumeric())
            {
                throw new ArgumentException("Type must be a numeric type!");
            }

            s = s.Trim();

            if (s.Length == 0)
            {
                return null;
            }

            StringBuilder cleanedNumber = new StringBuilder();
            foreach (char c in s)
            {
                switch (c)
                {
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                    case '-':
                    case '+':
                    case '.':
                    case 'x':
                    case 'X':
                    case 'e':
                    case 'E':
                        cleanedNumber.Append(c);
                        break;
                    case ',':
                        cleanedNumber.Append('.');
                        break;
                }
            }

            s = cleanedNumber.ToString();

            switch (Type.GetTypeCode(type))
            {
                case TypeCode.SByte:
                    return Convert.ToSByte(s);
                case TypeCode.Byte:
                    return Convert.ToByte(s);
                case TypeCode.UInt16:
                    return Convert.ToUInt16(s);
                case TypeCode.UInt32:
                    return Convert.ToUInt32(s);
                case TypeCode.UInt64:
                    return Convert.ToUInt64(s);
                case TypeCode.Int16:
                    return Convert.ToInt16(s);
                case TypeCode.Int32:
                    return Convert.ToInt32(s);
                case TypeCode.Int64:
                    return Convert.ToInt64(s);
                case TypeCode.Decimal:
                    return Convert.ToDecimal(s);
                case TypeCode.Double:
                    return Convert.ToDouble(s);
                case TypeCode.Single:
                    return Convert.ToSingle(s);
                case TypeCode.Object:
                    if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        return Nullable.GetUnderlyingType(type).IsNumeric();
                        //return IsNumeric(Nullable.GetUnderlyingType(type));
                    }
                    throw new NotANumberException();
            }
            throw new NotANumberException();
        }
    }
}
