﻿using ch.f00.DataFileParser.Attributes;
using ch.f00.DataFileParser.Errors;
using ch.f00.DataFileParser.Exceptions;
using ch.f00.DataFileParser.Util;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ch.f00.DataFileParser.CSV
{
    /// <summary>
    /// This class implements the parsing of text files with character separated data ("CSV-Files")
    /// The parsed acceptes data according RFC 4180, with some exceptions:
    /// * Data spread accross multiple lines is not supported. (For instance this: "SomeData";"Data\nRestOfData"\n )
    /// * Lines preceeded by "//", "'" or "#" will be treaten as comments and will be ignored.
    /// This parser does not implement special treatment for not-realy-conform programs like Microsoft(r) Excel(tm)
    /// </summary>
    /// <typeparam name="T">The resulting POCO. May have a ColumnName attribute.</typeparam>
    public class CSVDataFileParser<T> : DataFileParser<T> where T : class, new()
    {
        readonly char delimiter;
        static Logger log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Creates a new instance with ',' as delimiter
        /// </summary>
        public CSVDataFileParser()
            : base()
        {
            delimiter = ',';
        }

        /// <summary>
        /// Creates a new instance with the given char as delimiter
        /// </summary>
        /// <param name="delimiter">The character which delimits the data in the CSV</param>
        public CSVDataFileParser(char delimiter)
            : base()
        {
            this.delimiter = delimiter;
        }

        public override IEnumerable<T> ParseFile(TextReader input, Func<T, T> proccessor = null)
        {
            if (input == null)
                throw new ArgumentNullException("input");
            var fieldNameToFieldInfoMap = GetProperties();

            Func<Dictionary<String, String>, T> processingCreator = valueMap =>
            {
                T data = new T();
                Type dataType = typeof(T);

                foreach (var fieldPair in fieldNameToFieldInfoMap)
                {
                    if (valueMap.ContainsKey(fieldPair.Key))
                    {
                        PropertyInfo field = fieldPair.Value;
                        String value = valueMap[fieldPair.Key];
                        if (field.PropertyType.IsNumeric())
                        {
                            field.SetValue(data, value.ParseNumber(field.PropertyType), null);
                        }
                        else if (field.PropertyType.IsDate())
                        {
                            var dateFormatAttrib = field.GetCustomAttributes(typeof(DateTimeFormat), true).Cast<DateTimeFormat>();
                            var timestampAttrib = field.GetCustomAttributes(typeof(Timestamp), true).Cast<Timestamp>();

                            if (dateFormatAttrib.Count() > 0)
                            {
                                field.SetValue(data, value.ParseDate(dateFormatAttrib.First().FormatProvider), null);
                            }
                            else if (timestampAttrib.Count() > 0)
                            {
                                field.SetValue(data, value.ParseTimestamp(), null);
                            }
                            else
                            {
                                field.SetValue(data, value.ParseDate(), null);
                            }
                        }
                        else
                        {
                            field.SetValue(data, value, null);
                        }
                        valueMap.Remove(fieldPair.Key);
                    }
                    else
                    {
                        log.Info("Can't map {0} of {1} to a header of the column.", fieldPair.Key, dataType.Name);
                        if (base.ThrowOnTypeMembersMissmatch)
                        {
                            throw new MemberNotInFileHeaderException(fieldPair.Key);
                        }
                    }
                    if (base.ThrowOnFileHeadersMissmatch && valueMap.Count > 0)
                    {
                        throw new FileHeaderNotInMemberException(valueMap.First().Key);
                    }
                }

                if (proccessor != null)
                {
                    return proccessor(data);
                }
                else
                {
                    return data;
                }
            };

            return ParseFile(input, processingCreator);
        }

        public override IEnumerable<T> ParseFile(TextReader input, Func<Dictionary<String, String>, T> creator)
        {
            if (input == null)
                throw new ArgumentNullException("input");
            if (creator == null)
                throw new ArgumentNullException("creator");

            ParsingErrors.Clear();

            var resultingList = new LinkedList<T>();
            int lineCount = 0;

            string line = ReadUntilData(ref lineCount, input);

            if (line == null)
            {
                log.Info("The input seems to be empty or contains only commented text.");
                return default(IEnumerable<T>);
            }

            // match columns to class
            String[] headers = parseLine(line);

            for (lineCount++; (line = ReadUntilData(ref lineCount, input)) != null; lineCount++)
            {
                String[] tokens = parseLine(line);

                var valueDict = new Dictionary<String, String>(tokens.Length);

                for (int i = 0; i < tokens.Length; i++)
                {
                    valueDict.Add(headers[i], tokens[i]);
                }

                log.Trace("Handing conversion of line {0} into an object over to the converter given.", lineCount);

                try
                {
                    T record = creator(valueDict);

                    resultingList.AddLast(record);
                }
                catch (FileHeaderNotInMemberException e)
                {
                    throw e;
                }
                catch (MemberNotInFileHeaderException e)
                {
                    throw e;
                }
                catch (Exception e)
                {
                    ParsingErrors.AddLast(new LineError()
                    {
                        LineNumber = lineCount,
                        RelatedException = e,
                        AffectedLine = line
                    });
                }

                log.Trace("Line {0} parsed successfully.", lineCount);
            }

            return resultingList;
        }

        private string[] parseLine(string line)
        {
            List<string> tokens = new List<string>();
            StringBuilder buffer = new StringBuilder();
            bool quoted = false;

            for (int i = 0; i < line.Length; i++)
            {
                char c0 = line[i];

                if (c0 == '"')
                {
                    char c1 = (i == line.Length ? '\0' : line[i + 1]);

                    if (c1 == '"' && quoted)
                    {
                        buffer.Append('"');
                        i++;
                    }
                    else
                    {
                        quoted = !quoted;
                    }
                }
                else if (!quoted && c0 == delimiter)
                {
                    tokens.Add(buffer.ToString());
                    buffer = new StringBuilder();
                }
                else
                {
                    buffer.Append(c0);
                }
            }

            // if the line did not end on DELIMITER
            if (buffer.Length > 0)
            {
                tokens.Add(buffer.ToString());
            }

            return tokens.ToArray();
        }
    }
}
