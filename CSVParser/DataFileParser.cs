﻿using ch.f00.DataFileParser.Attributes;
using ch.f00.DataFileParser.Errors;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ch.f00.DataFileParser
{
    /// <summary>
    /// The general interface of a parser that parses text with formated data.
    /// </summary>
    /// <typeparam name="T">The resulting POCO. May have a ColumnName attribute.</typeparam>
    public abstract class DataFileParser<T> where T : class, new()
    {
        private static Logger log = LogManager.GetCurrentClassLogger();
        public LinkedList<LineError> ParsingErrors { get; private set; }

        private bool throwOnFileHeaderMissmatch = false;
        private bool throwOnTypeMembersMissmatch = false;
        private bool skipComments = true;

        public DataFileParser()
        {
            ParsingErrors = new LinkedList<LineError>();
        }

        /// <summary>
        /// Checks if there were parsing errors
        /// </summary>
        /// <returns>True if there were parsing errors</returns>
        public bool HasParsingError()
        {
            return ParsingErrors.Count > 0;
        }

        /// <summary>
        /// Set to true if you want the parser to throw an exception if the file has columns,
        /// which can't be matched to a member of the given type T.
        /// Default: false
        /// </summary>
        public bool ThrowOnFileHeadersMissmatch
        {
            get { return throwOnFileHeaderMissmatch; }
            set { throwOnFileHeaderMissmatch = value; }
        }

        /// <summary>
        /// Set to true if you want the parser to throw an exception if the given type T has members,
        /// which can't be matched to a column of the file.
        /// Default: false
        /// </summary>
        public bool ThrowOnTypeMembersMissmatch
        {
            get { return throwOnTypeMembersMissmatch; }
            set { throwOnTypeMembersMissmatch = value; }
        }

        /// <summary>
        /// Set to false if you don't want that the parser skips commented lines.
        /// Default: true
        /// </summary>
        public bool SkipComments
        {
            get { return skipComments; }
            set { skipComments = value; }
        }

        /// <summary>
        /// Parses the file one entry by another, and invokes the given processor on every entry.
        /// The parser first parses an entry into the type T, and then calls the processor with the just created object as reference.
        /// </summary>
        /// <param name="filename">The file to be parsen.</param>
        /// <param name="proccessor">Do special processing on the type.</param>
        /// <param name="skipComments">Skip commented lines in the input (i.e. lines starting with "//", "#", "'").</param>
        /// <returns>An enumerable of objects processed by the proccessor</returns>
        public IEnumerable<T> ParseFile(String filename, Func<T, T> proccessor = null)
        {
            if (filename == null)
                throw new ArgumentNullException("filename");
            log.Trace("Parsing file {0}.", filename);
            return ParseFile(new StreamReader(filename, true), proccessor);
        }

        /// <summary>
        /// Parses the text one entry by another, and invokes the given processor on every entry.
        /// The parser first parses an entry into the type T, and then calls the processor with the just created object as reference.
        /// </summary>
        /// <param name="reader">The text to be parsen.</param>
        /// <param name="proccessor">Do special processing on the type.</param>
        /// <param name="skipComments">Skip commented lines in the input (i.e. lines starting with "//", "#", "'").</param>
        /// <returns>An enumerable of objects processed by the proccessor</returns>
        public abstract IEnumerable<T> ParseFile(TextReader input, Func<T, T> proccessor = null);

        /// <summary>
        /// Parses the file one entry by another, and invokes the given converter on every entry.
        /// The parser first parses an entry into the type T, and then calls the converter with the object just created expecting a new object with the type TResulte from the converter.
        /// </summary>
        /// <param name="filename">The file to be parsen.</param>
        /// <param name="creator">Converts between the incomming text tokens and an objects.</param>
        /// <param name="skipComments">Skip commented lines in the input (i.e. lines starting with "//", "#", "'").</param>
        /// <returns>An enumerable of objects converted to the given type</returns>
        public IEnumerable<T> ParseFile(String filename, Func<Dictionary<String, String>, T> creator)
        {
            if (filename == null)
                throw new ArgumentNullException("filename");
            if (creator == null)
                throw new ArgumentNullException("creator");
            log.Trace("Parsing file {0}.", filename);
            return ParseFile(new StreamReader(filename, true), creator);
        }

        /// <summary>
        /// Parses the tex one entry by another, and invokes the given converter on every entry.
        /// The parser first parses an entry into the type T, and then calls the converter with the object just created expecting a new object with the type TResulte from the converter.
        /// </summary>
        /// <param name="reader">The text to be parsen.</param>
        /// <param name="creator">Converts between the incomming text tokens and an objects.</param>
        /// <param name="skipComments">Skip commented lines in the input (i.e. lines starting with "//", "#", "'").</param>
        /// <returns>An enumerable of objects converted to the given type</returns>
        public abstract IEnumerable<T> ParseFile(TextReader input, Func<Dictionary<String, String>, T> creator);

        protected string ReadUntilData(ref int lineCounter, TextReader input)
        {
            string line;
            for (; (line = input.ReadLine()) != null; lineCounter++)
            {
                if (skipComments && (line.StartsWith("#") || line.StartsWith("//") || line.StartsWith("'")))
                {
                    log.Trace("Skipped a line because it started with \" {0} \"", line[0]);
                    continue;
                }
                else
                {
                    return line;
                }
            }
            return null;
        }

        protected Dictionary<String, PropertyInfo> GetProperties()
        {
            var propertiesOfType = typeof(T).GetProperties();
            var properties = new Dictionary<String, PropertyInfo>(propertiesOfType.Length);
            foreach (var field in propertiesOfType)
            {
                var colNameAttrib = field.GetCustomAttributes(typeof(ColumnName), true).Cast<ColumnName>();

                if (colNameAttrib.Count() > 0)
                {
                    properties.Add(colNameAttrib.First().AlternativeColumnName, field);
                }
                else
                {
                    properties.Add(field.Name, field);
                }
            }

            return properties;
        }
    }
}
